
#!/bin/bash
#

function _helpDefaultWrite()
{
    VAL=$1
    local VAL1=$2

    if [ ! -z "$VAL" ] || [ ! -z "$VAL1" ]; then
    defaults write "${ScriptHome}/Library/Preferences/macosunlocker.slsoft.de.plist" "$VAL" "$VAL1"
    fi
}

function _helpDefaultRead()
{
    VAL=$1

    if [ ! -z "$VAL" ]; then
    defaults read "${ScriptHome}/Library/Preferences/macosunlocker.slsoft.de.plist" "$VAL"
    fi
}

ScriptHome=$(echo $HOME)
ScriptTmpPath="$HOME"/.mu_temp
#NodeId=$( _helpDefaultRead "NodeId" )
volume_name=$( diskutil info "$NodeId" | grep "Volume Name" | sed 's/.*://g' | xargs )
MY_PATH="`dirname \"$0\"`"
cd "$MY_PATH"

function _get_node()
{

    NodeId2=$( mount | grep ".mu_temp/mount" )
    if [[ "$NodeId2" != "" ]]; then
        _helpDefaultWrite "RW" "Yes"
        NodeId=$( mount | grep "on / (" )
        NodeId=$( echo "$NodeId" | sed 's/on \/.*//g' | rev | cut -c 4- | rev )
        _helpDefaultWrite "NodeId" "$NodeId"
        exit
    fi

    NodeId=$( mount | grep "on / (" )

    if [[ "$NodeId" = *"read-only"* ]]; then
        _helpDefaultWrite "RW" "No"
    else
        _helpDefaultWrite "RW" "Yes"
    fi
    
    NodeId=$( echo "$NodeId" | sed 's/on \/.*//g' | rev | cut -c 4- | rev )
    #NodeId=$( echo "$NodeId" | sed 's/on \/.*//g' )
    
    _helpDefaultWrite "NodeId" "$NodeId"
}

function _set_rw()
{
    if [ ! -d "$ScriptTmpPath" ]; then
        mkdir "$ScriptTmpPath"
        mkdir "$ScriptTmpPath"/mount
    fi
    
    osascript -e 'do shell script "sudo mount -o nobrowse -t apfs '"'$NodeId'"' '"'$ScriptTmpPath'"'/mount" with administrator privileges' #>/dev/null 2>&1
    ln -s "$ScriptTmpPath"/mount "$HOME"/Desktop/"$volume_name"-RW
}

function _apply_reboot()
{
    rm "$HOME"/Desktop/"$volume_name"-RW
    osascript -e 'do shell script "sudo bless --folder '"'$ScriptTmpPath'"'/mount/System/Library/CoreServices --bootefi --create-snapshot; shutdown -r now" with administrator privileges'
}

function _check_authroot()
{
    AuthRoot=$( csrutil authenticated-root status )
    
    if [[ "$AuthRoot" = *"enabled"* ]]; then
        _helpDefaultWrite "AuthRoot" "Yes"
    else
        _helpDefaultWrite "AuthRoot" "No"
    fi
}

$1
