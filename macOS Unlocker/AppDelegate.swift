//
//  AppDelegate.swift
//  macOS Unlocker
//
//  Created by Prof. Dr. Luigi on 11.11.20.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    


    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        UserDefaults.standard.removeObject(forKey: "NodeId")
        UserDefaults.standard.removeObject(forKey: "RW")
        UserDefaults.standard.removeObject(forKey: "AuthRoot")
    }


}

